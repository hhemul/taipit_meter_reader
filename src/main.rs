extern crate regex;
extern crate structopt;
extern crate serial;

mod connection;
pub mod parser;

use structopt::StructOpt;
use std::io::{self, BufRead};
use regex::Regex;

#[derive(StructOpt)]
pub struct Opts {
    #[structopt(short = "p", long = "port")]
    port: Option<String>,
    #[structopt(long = "sn")]
    serial_number: Option<String>,
    #[structopt(short = "r", long = "read_stdin")]
    read_stdin: bool,
    #[structopt(long = "calc_bcc")]
    calc_bcc: Option<String>
}

fn print_hchar(char1: char, char0: char) -> usize {
    let ch: u32 = (char1.to_digit(16).unwrap() << 4) + char0.to_digit(16).unwrap();
    if ch < 32 {
        print!("\\x{}{} ", char1, char0);
    } else {
        print!("{}", unsafe { std::char::from_u32_unchecked(ch) });
    }
    0
}

fn decode(text: &str) {
    let mut hchars = ['\\', 'x', '\0', '\0'];
    let mut i = 0;
    for ch in text.chars() {
        let ch_h = hchars[i];
        if i < 2 {
            if ch_h == ch {
                i += 1;
                continue;
            }
        } else {
            if ch.is_digit(16) {
                hchars[i] = ch;
                i += 1;
                if i >= hchars.len() { i = print_hchar(hchars[2], hchars[3]); }
                continue;
            }
        }
        for j in 0..i { print!("{}", hchars[j]); }
        i = 0;
        print!("{}", ch);
    }
}

fn test(regex: &Regex, text: &str, prefix: &str) {
    if let Some(caps) = regex.captures(text) {
        print!("{} {} ", &caps[1], prefix);
        decode(&caps[2]);
        println!("");
    }
} 

fn main() {
    let opts = Opts::from_args();
    if opts.read_stdin {
        let regex_write = Regex::new("^(\\S+)\\s+write\\(14,\\s+\"([^\"]*)\".*$").unwrap();
        let regex_read = Regex::new("^(\\S+)\\s+read\\(14,\\s+\"([^\"]*)\".*$").unwrap();
        let stdin = io::stdin();
        for line_result in stdin.lock().lines() {
            if let Ok(line) = line_result {
                test(&regex_write, &line, "W");
                test(&regex_read, &line, "R");
            }
        }
    } else if let Some(ref port_path) = opts.port {
        let mut port = serial::open(port_path).unwrap();
        connection::interact(&mut port, opts.serial_number).unwrap();
    } else if let Some(ref str_to_calc) = opts.calc_bcc {
        let code = connection::calc_bcc(str_to_calc);
        println!("Block check character (BCC) of \"{}\" {} - \"{}\"", str_to_calc, code, unsafe { std::char::from_u32_unchecked(code) });
    }
}
