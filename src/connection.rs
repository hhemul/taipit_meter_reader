use std::time::Duration;
use std::io::{Write, Read, Result, Error, ErrorKind};
use serial::prelude::*;
use crate::parser::*;

struct BlockControlChar<'a> {
    port: &'a mut dyn SerialPort,
    bcc: u8,
    within_block: bool
}

impl<'a> Write for BlockControlChar<'a> {
    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        for b in buf {
            if !self.within_block {
                if *b == 0x1 || *b == 0x2 { self.within_block = true; }
            } else {
                self.bcc ^= b;
                if *b == 0x3 { self.within_block = false; }
            }
        }
        self.port.write(buf)
    }

    fn flush(&mut self) -> Result<()> {
        self.port.flush()
    }
}

pub fn interact<T: SerialPort>(port: &mut T, serial_number: Option<String>) -> Result<()> {
    let mut buffer = [0; 128];

    port.reconfigure(&|settings| {
        settings.set_baud_rate(serial::Baud300)?;
        settings.set_char_size(serial::Bits7);
        settings.set_parity(serial::ParityNone);
        settings.set_stop_bits(serial::Stop1);
        settings.set_flow_control(serial::FlowNone);
        Ok(())
    })?;

    port.set_timeout(Duration::from_millis(1000))?;

    write!(port, "/?{}!\r\n", serial_number.unwrap_or("".to_string()))?;

    Ok(())
}

fn write<T: SerialPort>(port: &mut T, addr: u32, value: &str) -> Result<()> {
    ;
    Ok(())
}

fn read<T: SerialPort>(port: &mut T, buf: &mut [u8]) -> Result<(String, String)> {
    let mut result = ReadResult::initial();
    loop {
        match port.read(&mut buf[result.pos..]) {
            Ok(n) => {
                parse_read_buffer(&mut buf[result.pos..result.pos + n], &mut result);
                if result.block_sum != 0 { return Err(Error::new(ErrorKind::InvalidData, "Block checksum error")); }
                if result.state == ReadResultState::Complete { break };
            },
            Err(e) => match e.kind() {
                ErrorKind::Interrupted => {},
                ErrorKind::WouldBlock => {},
                _ => return Err(e)
            }
        }
    }

    let header = std::str::from_utf8(&buf[result.head.0..result.head.1]);
    if header.is_err() { return Err(Error::new(ErrorKind::InvalidData, header.unwrap_err())); }
    let text = std::str::from_utf8(&buf[result.text.0..result.text.1]);
    if text.is_err() { return Err(Error::new(ErrorKind::InvalidData, text.unwrap_err())); }

    Ok((String::from(header.unwrap()), String::from(text.unwrap())))
}

pub fn calc_bcc(s: &str) -> u32 {
    let mut sum: u32 = 0;
    for code_point in s.chars() {
        sum = sum ^ (code_point as u32);
    }
    sum
}
