#[derive(PartialEq, Debug)]
pub enum ReadResultState {
    Initial, Header, Text, BCC, Complete
}

pub struct ReadResult {
    pub pos: usize,
    pub state: ReadResultState,
    pub head: (usize, usize),
    pub text: (usize, usize),
    pub etx: u8,
    pub bcc: u8,
    pub block_sum: u8,
    pub end: usize,
}

impl ReadResult {
    pub fn initial() -> Self {
        Self {
            pos: 0,
            state: ReadResultState::Initial,
            head: (0, 0),
            text: (0, 0),
            etx: 0,
            bcc: 0,
            block_sum: 0,
            end: 0,
        }
    }
}

pub fn parse_read_buffer(buf: &[u8], result: &mut ReadResult) {
    for (i, &b) in buf.iter().enumerate() {
        match result.state {
            ReadResultState::Header => {
                result.head.1 = i;
                result.block_sum ^= b;
            },
            ReadResultState::Text => {
                result.text.1 = i;
                result.block_sum ^= b;
            },
            ReadResultState::BCC => {
                result.block_sum ^= b;
                result.bcc = b;
                result.end = i;
                result.state = ReadResultState::Complete;
                break;
            },
            _ => ()
        }
        match b {
            0x01 => {
                result.state = ReadResultState::Header;
                result.head.0 = i + 1;
                result.head.1 = result.head.0;
            },
            0x02 => {
                result.state = ReadResultState::Text;
                result.text.0 = i + 1;
                result.text.1 = result.text.0;
            },
            0x03 => {
                result.etx = b;
                result.state = ReadResultState::BCC;
            },
            _ => ()
        }
    }
    result.pos += buf.len();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test0() {
        let s = b"\x0260010AFF(0000000000000000)\x03t";
        let mut result = ReadResult::initial();
        parse_read_buffer(s, &mut result);

        assert_eq!(result.pos, 29);
        assert_eq!(result.state, ReadResultState::Complete);
        assert_eq!(result.head, (0, 0));
        assert_eq!(result.text, (1, 27));
        assert_eq!(result.etx, 3);
        assert_eq!(result.bcc, 116);
        assert_eq!(result.block_sum, 0);
        assert_eq!(result.end, 28);
    }

    #[test]
    fn test1() {
        let s = b"\x01P0\x02(00000000)\x03`";
        let mut result = ReadResult::initial();
        parse_read_buffer(s, &mut result);

        assert_eq!(result.pos, 16);
        assert_eq!(result.state, ReadResultState::Complete);
        assert_eq!(result.head, (1, 3));
        assert_eq!(result.text, (4, 14));
        assert_eq!(result.etx, 3);
        assert_eq!(result.bcc, 96);
        assert_eq!(result.block_sum, 0);
        assert_eq!(result.end, 15);
    }
}